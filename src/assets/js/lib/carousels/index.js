import $ from 'jquery'
import 'slick-carousel'

export default () => {
	$("#staff-slider").slick({

	  slidesToShow: 2,
	  slidesToScroll: 2,
	  autoplay: false,
	  arrows: true,
	  prevArrow: `<button class="sl_button--carousel sl_prev"></button>`,
	  nextArrow: `<button class="sl_button--carousel sl_next"></button>`,
	  swipeToSlide: true,
	  infinite: true,
	  dots: true,
	  responsive: [
	    {
	      breakpoint: 768,
	      settings: {
	        slidesToShow: 1
	      }
	    }
	  ]
	});

	$("#testimonial-slider").slick({

	  slidesToShow: 1,
	  slidesToScroll: 1,
	  autoplay: false,
	  arrows: true,
	  prevArrow: `<button class="sl_button--carousel sl_prev"></button>`,
	  nextArrow: `<button class="sl_button--carousel sl_next"></button>`,
	  swipeToSlide: true,
	  infinite: true,
	  responsive: [
	    {
	      breakpoint: 768,
	      settings: {
	        slidesToShow: 1,
	        arrows: false,
	        autoplay: true,
  			autoplaySpeed: 4000,
	      }
	    }
	  ]
	});

	$("#location-slider-slider").slick({

		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: false,
		arrows: true,
		dots: true,
		prevArrow: `<button class="sl_button--carousel sl_prev"></button>`,
		nextArrow: `<button class="sl_button--carousel sl_next"></button>`,
		swipeToSlide: true,
		infinite: true,
		responsive: [
		  {
			breakpoint: 768,
			settings: {
			  slidesToShow: 1,
			  arrows: false,
			  autoplay: true,
				autoplaySpeed: 4000,
			}
		  }
		]
	  });

	$("#the-oaks-slider").slick({

		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: false,
		arrows: true,
		dots: true,
		prevArrow: `<button class="sl_button--carousel sl_prev"></button>`,
		nextArrow: `<button class="sl_button--carousel sl_next"></button>`,
		swipeToSlide: true,
		infinite: true,
		responsive: [
		  {
			breakpoint: 768,
			settings: {
			  slidesToShow: 1,
			  arrows: false,
			  autoplay: true,
				autoplaySpeed: 4000,
			}
		  }
		]
	  });

	  $("#the-landing-slider").slick({

		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: false,
		arrows: true,
		dots: true,
		prevArrow: `<button class="sl_button--carousel sl_prev"></button>`,
		nextArrow: `<button class="sl_button--carousel sl_next"></button>`,
		swipeToSlide: true,
		infinite: true,
		responsive: [
		  {
			breakpoint: 768,
			prevArrow: `<button class="sl_button--carousel sl_prev"></button>`,
			nextArrow: `<button class="sl_button--carousel sl_next"></button>`,
			settings: {
			  slidesToShow: 1,
			  arrows: false,
			  autoplay: true,
				autoplaySpeed: 4000,
			}
		  }
		]
	  });

	  $("#brunswick-place-slider").slick({

		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: false,
		arrows: true,
		dots: true,
		prevArrow: `<button class="sl_button--carousel sl_prev"></button>`,
		nextArrow: `<button class="sl_button--carousel sl_next"></button>`,
		swipeToSlide: true,
		infinite: true,
		responsive: [
		  {
			breakpoint: 768,
			settings: {
			  slidesToShow: 1,
			  arrows: false,
			  autoplay: true,
				autoplaySpeed: 4000,
			}
		  }
		]
	  });
};
